<?php
/**
 * Blog Archive Template
 * @package NPGW
 * @since 0.0.1
 */

get_header(); ?>

<div class='outer'>
<div class='container'>
<div class='loop'>

<?php npgw_intro_title(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div <?php post_class(); ?>>

		<?php the_title( '<a href=\'' . get_permalink() . '\'><h1 class=\'page-title\'>', '</h1></a>' ); ?>

		<div class='content'>

			<?php the_excerpt(); ?>

		</div>

	</div>

<?php endwhile; endif; wp_reset_query(); ?>

</div>

<?php get_sidebar(); ?>

</div>
</div>

<?php get_footer();