module.exports = function( grunt ) {
	'use strict';

	grunt.initConfig( {
		pkg: grunt.file.readJSON( 'package.json' ),
		concat: {
			options: {
				stripBanners: true
			},
			admin: {
				src: ['assets/js/src/fitvids.js','assets/js/src/admin.js'],
				dest: 'assets/js/debug/admin.js'
			},
			header: {
				src: ['assets/js/src/respond.js'],
				dest: 'assets/js/debug/header.js'
			},
			footer: {
				src: ['assets/js/src/fitvids.js','assets/js/src/footer.js'],
				dest: 'assets/js/debug/footer.js'
			},
			contact: {
				src: ['assets/js/src/contact.js','assets/js/src/contact.js'],
				dest: 'assets/js/debug/contact.js'
			}
		},
		jshint: {
			all: [
				'Gruntfile.js',
				'assets/js/src/*.js'
			],
			options: {
				jshintrc: '.jshintrc'
			}
		},
		uglify: {
			all: {
				files: {
					'assets/js/min/admin.js': ['assets/js/debug/admin.js'],
					'assets/js/min/header.js': ['assets/js/debug/header.js'],
					'assets/js/min/footer.js': ['assets/js/debug/footer.js'],
					'assets/js/min/contact.js': ['assets/js/debug/contact.js']
				},
				options: {
					mangle: {
						except: ['jQuery']
					}
				}
			}
		},
		compass:   {
			debug: {
				options: {
					config: 'config.rb',
					cssDir: 'assets/css/debug',
					outputStyle: 'expanded',
					noLineComments: true
				}
			},
			min: {
				options: {
					config: 'config.rb',
					cssDir: 'assets/css/min',
					outputStyle: 'compressed',
					noLineComments: true
				}
			}
		},
		watch:  {
			compass: {
				files: ['assets/css/src/**/*.scss'],
				tasks: ['compass:debug', 'compass:min'],
				force: true
			},
			scripts: {
				files: ['assets/js/src/**/*.js', 'assets/js/src/plugins/**/*.js'],
				tasks: ['jshint', 'concat', 'uglify'],
				force: true,
				options: {
					debounceDelay: 500
				}
			}
		}
	} );

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	
	grunt.registerTask( 'default', ['jshint', 'concat', 'uglify', 'compass'] );

	grunt.util.linefeed = '\n';
};