<?php
/**
 * Admin functions
 *
 * @package NPGW
 * @author 10up / Jeff Sebring <jeff@10up.com>
 */

// Instantiate!
NPGW_Admin::instance();

class NPGW_Admin {

	/**
	 * Instance
	 * @var NPGW_Admin
	 * @access protected
	 */
	protected static $_instance;

	/**
	 * Singleton instance
	 * @return NPGW_Admin
	 */
	public static function instance() {

		if ( ! isset( self::$_instance ) )
			self::$_instance = new self;

		return self::$_instance;

	}

	/**
	 * Theme constructor
	 * @access protected
	 */
	protected function __construct() {

		// Actions
		add_action( 'wp_dashboard_setup', array( $this, 'dashboard' ) );
		add_action( 'save_post', array( $this, 'save_post' ) );
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_assets' ) );

	}

	/**
	 * Remove Dashboard Metaboxes
	 * @return void
	 * @access public
	 */
	public function dashboard() {

		// Clean dashboard
		remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );

	}

	/**
	 * Admin Scripts & styles
	 * @return void
	 * @access public
	 */
	public function admin_assets() {

		$template_directory_uri = get_template_directory_uri();

		wp_enqueue_style(
			'npgw-admin',
			$template_directory_uri . '/assets/css/min/admin.css'
		);

		wp_enqueue_script(
			'npgw-admin',
			$template_directory_uri . '/assets/js/min/admin.js',
			array( 'jquery' ),
			'',
			true
		);

	}

	/**
	 * Add metabox for metadata selections
	 * @return bool metadata input html
	 * @access public
	 */
	public function add_meta_boxes() {

		$post = get_post( get_the_ID() );

		if ( get_the_ID() == get_option( 'page_on_front' )
			|| $post->post_name === 'about-fhra' ) {

			add_meta_box( 'npgw_page_meta', 'Custom Content', array( $this, 'page_meta' ), 'page', 'advanced', 'high' );

		}

	}

	/**
	 * Add metabox fields for metadata selections
	 * @return bool metadata input html
	 * @access public
	 */
	public function page_meta() {

		wp_nonce_field( 'npgw', 'npgw_page_meta' );

		$post = get_post( get_the_ID() );

		if ( get_the_ID() == get_option( 'page_on_front' ) ) {

			self::textarea( 'npgw_home_welcome', 'Welcome', 'This is the text just under the welcome image, with the tan background.' );

		} elseif ( $post->post_name === 'about-fhra' ) {

			self::textarea( 'npgw_about_fhr_production', 'Production Info' );
			self::textarea( 'npgw_about_fhr_community', 'Community Info' );
			self::textarea( 'npgw_about_fhr_safety', 'Safety Info' );
			self::textarea( 'npgw_about_fhr_agencies', 'Local Agencies' );
			self::textarea( 'npgw_about_fhr_approach', 'FHRA Approach' );
			self::textarea( 'npgw_about_fhr_response', 'Response' );
		}

	}

	/**
	 * Exgdable Metabox textarea
	 * @param string $name input name
	 * @param string $title input title
	 * @param string $description Help text for input
	 * @return string metadata textarea html
	 * @access private
	 * @static
	 */
	private static function textarea( $name = null, $title = null, $description = null ) {

		if ( ! is_string( $name ) )
			return;

		if ( is_string( $title ) )
			echo "<h4>{$title}</h4>";

		if ( is_string( $description ) )
			echo "<p><small>{$description}</small></p>";

		if ( ! $value = get_post_meta( get_the_ID(), $name, true ) )
			$value = null; ?>

		<textarea rows="1" cols="40" name="<?php echo esc_attr( $name ); ?>"><?php echo wp_kses_post( $value ) ?></textarea>
		<br>

		<?php

	}

	/**
	 * Add metabox fields for metadata selections
	 * @param string $name input name
	 * @param string $title input title
	 * @param string $description Help text for input
	 * @return string metadata textarea html
	 * @access private
	 * @static
	 */
	private static function text_input( $name = null, $title = null, $description = null ) {

		if ( ! is_string( $name ) )
			return;

		if ( is_string( $title ) )
			echo "<h4>{$title}</h4>";

		if ( is_string( $description ) )
			echo "<p><small>{$description}</small></p>";

		if ( ! $value = get_post_meta( get_the_ID(), $name, true ) )
			$value = null; ?>

		<input class="large-text" type="type" rows="1" cols="40" name="<?php echo esc_attr( $name ); ?>" value="<?php echo wp_kses_post( $value ) ?>" />
		<br>

		<?php

	}

	/**
	 * Save metadata from Post Submit box inputs
	 * @param  integer $post_id
	 * @return bool true on success
	 * @access public
	 */
	public function save_post( $post_id ) {

		if ( ! in_array( get_post_type( $post_id ), array( 'page' ) ) )
			return false;

		// Skip Autosaves
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || wp_is_post_revision( $post_id ) )
			return false;

		// Check user capability
		if ( ! current_user_can( 'edit_post', $post_id ) )
			return false;

		// Check the nonce
		if ( ! isset( $_POST[ 'npgw_page_meta' ] )
			|| ! wp_verify_nonce( $_POST[ 'npgw_page_meta' ], 'npgw' ) )
				return false;

		$text_value_whitelist = array(
			'npgw_home_welcome',
			'npgw_about_fhr_production',
			'npgw_about_fhr_community',
			'npgw_about_fhr_safety',
			'npgw_about_fhr_agencies',
			'npgw_about_fhr_approach',
			'npgw_about_fhr_response',
		);
		
		foreach ( $text_value_whitelist as $textarea ) {

			if ( isset( $_POST[ $textarea ] ) ) {

				update_post_meta( $post_id, $textarea, wp_kses_post( $_POST[ $textarea ] ) );
			
			}

		}

		return true;

	}

}