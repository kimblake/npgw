<?php
/**
 * Template tags
 * @package NPGW
 * @since 0.0.1
 */

/**
 * Comment list callback
 * @return string comments html
 * @see wp_list_comments()
 */
function npgw_comments( $comment, $args, $depth ) {

	global $comment;

	extract( $args, EXTR_SKIP ); ?>

	<div <?php comment_class( empty( $args[ 'has_children' ] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID(); ?>">

		<?php echo get_avatar( $comment, 75 ); ?>

		<span class="comment_author"><?php comment_author_link(); ?> on <time datetime="<?php comment_date( 'Y-m-d' ); ?>"><?php comment_date(); ?></time></span>

	<?php if ( ! $comment->comment_approved ) : ?>

		<p class="comment-awaiting-moderation">Your comment is awaiting moderation.</p>

	<?php endif; ?>

	<div class="content comment_content">

		<?php comment_text(); ?>

	</div>

	<div class="reply">

		<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args[ 'max_depth' ] ) ) ); ?>

	</div>

	<?php

}

/**
 * Is a feature page being displayed?
 * @return bool True if showing a feature page
 */
function npgw_is_feature_page() {

	$feature_pages = array(
		'about-fhra', 
		'about-sulfolane',
		'fhra-results',
		'residents',
		'realtors',
		'contact',
		'faqs',
		'resources'
	);

	if ( is_front_page() || is_page( $feature_pages ) )
		return true;

	return false;

}

/**
 * Theme Image output wrapper
 * @param  string $image image
 * @return string
 */
function npgw_image( $image = null ) {

	if ( ! $image )
		return false;

	return get_template_directory_uri() . "/assets/images/{$image}";

}

/**
 * Return the private wells map custom field with fallback if no image specified.
 * @return string
 */
function npgw_private_wells_map_url() {
	global $post;
	$private_wells_map = get_post_meta( $post->ID, 'private-wells-map', true );
	$image_url = ( $private_wells_map ) ? $private_wells_map : npgw_image('private-wells-map.png');

	return esc_url( $image_url );
}

/**
 * Archive intro title
 * @return string archive intro title
 */
function npgw_intro_title() {
	
	$title = null;

	$append_date = ' Archive';

	if ( is_search() ) {

		if ( have_posts() ) {

			$title = 'Search Results For "' . get_search_query() . '"';
		
		} else {

			$title = 'No Search Results Found For "' . get_search_query() . '"';

		}

	} elseif ( is_date() ) {

		if ( is_day() ) {

			$title = get_the_time( 'F jS, Y' ) . $append_date;

		} elseif ( is_month() ) {

			$title = single_month_title( ' ', false ) . $append_date;

		} elseif ( is_year() ) {

			$title = the_time( 'Y' ) . $append_date;

		}

	} elseif ( is_category() || is_tag() || is_tax() ) {

		$title = single_term_title( '', false );

	} elseif ( is_post_type_archive() ) {

		$title = post_type_archive_title( '', false );

	} elseif ( is_author() ) {

		global $authordata;
		$author_id = $authordata->data->ID;

		$title = get_the_author_meta( 'display_name', $author_id );

	}

	if ( ! $title )
		return false; ?>

	<h1><?php echo $title; ?></h1>

	<?php return true;

}

/**
 * Archive intro description
 * @return string archive intro description
 */
function npgw_intro_description() {

	$description = null;

	if ( is_category() || is_tag() || is_tax() ) {

		$description = wpautop( term_description() );

	} elseif ( is_author() ) {

		global $authordata;
		$author_id = $authordata->data->ID;
		
		echo get_avatar( $author_id, 170 );

		$description = get_the_author_meta( 'description', $author_id );
		if ( preg_match( '/^.{1,200}\b/s', $description, $matches ) ) {
			$description = $matches[0] . '...';
		}

	}

	if ( ! $description )
		return false; ?>

	<div class="intro-description content clearfix">

		<?php echo $description; ?>
	
	</div>

	<?php return true;

}



/**
 * Page links
 * @return string link pages html
 * @access public
 * @static
 */
function npgw_link_pages() {

	wp_link_pages( array(
		'before' => "<div class=\"page-links\">\n<span>Pages: </span>",
		'after' =>  "\n</div>"
	) );

}

/**
 * Next and prevous post links
 * @return string next previous posts html
 * @access public
 * @static
 */
function npgw_adjacent_posts() {

	$links = null;

	ob_start();
	previous_post_link( '%link', 'Previous' );
	$prev_post = ob_get_clean();

	ob_start();
	next_post_link(  '%link', 'Next' );
	$next_post = ob_get_clean();

	if ( $prev_post )
		$links .= "\n<span class=\"prev-post\">\n$prev_post\n</span>\n";

	if ( $next_post )
		$links .= "\n<span class=\"next-post\">\n$next_post\n</span>\n";

	if ( ! $links )
		return false;

	echo "\n<nav class=\"adjacent-posts\">\n$links\n</nav>\n";

	return true;

}

/**
 * Next and prevous image links
 * @return string next previous image html
 */
function npgw_adjacent_images() {

	$links = null;

	ob_start();
	previous_image_link( '%link', 'Previous' );
	$prev_image = ob_get_clean();

	ob_start();
	next_image_link(  '%link', 'Next' );
	$next_image = ob_get_clean();

	if ( $prev_image )
		$links .= "\n<span class=\"prev-image\">\n$prev_image\n</span>\n";

	if ( $next_image )
		$links .= "\n<span class=\"next-image\">\n$next_image\n</span>\n";

	if ( ! $links )
		return false;

	echo "\n<nav class=\"adjacent-images\">\n$links\n</nav>\n";

	return true;

}

/**
 * Attached image with a link to the next attached image.
 * @return string attached image html
 */
function npgw_attached_image() {

	$post                = get_post();
	$attachment_size     = apply_filters( '_s_attachment_size', array( 1200, 1200 ) );
	$next_attachment_url = wp_get_attachment_url();

	/**
	 * Grab the IDs of all the image attachments in a gallery so we can get the
	 * URL of the next adjacent image in a gallery, or the first image (if
	 * we're looking at the last image in a gallery), or, in a gallery of one,
	 * just the link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID'
	) );

	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {

		foreach ( $attachment_ids as $attachment_id ) {

			if ( $attachment_id == $post->ID ) {

				$next_id = current( $attachment_ids );

				break;

			}

		}

		// get the URL of the next image attachment...
		if ( $next_id )
			$next_attachment_url = get_attachment_link( $next_id );

		// or get the URL of the first image attachment.
		else
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );

	}

	printf( '<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>',
		esc_url( $next_attachment_url ),
		the_title_attribute( array( 'echo' => false ) ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);

}

/**
 * Post Tags
 * @return string post tags html
 */
function npgw_post_tags() {

	the_tags( "\n<div class=\"post-tags\">Tagged: ", ", \n", "\n</div>\n" );

}

/**
 * Pagination links
 * @return string paginate html
 */
function npgw_paginate() {

	global $wp_query;

	$paginate_args[ 'base' ] = str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) );
	$paginate_args[ 'current' ]  = max( 1, get_query_var( 'paged' ) );
	$paginate_args[ 'total' ] = $wp_query->max_num_pages;
	$paginate_args[ 'next_text' ] = 'Next';
	$paginate_args[ 'prev_text' ] = 'Previous';

	$links = paginate_links( $paginate_args );

	if ( ! $links )
		return false; ?>

	<nav class="paginate-links">

		<?php echo $links; ?>
	
	</nav>

	<?php return true;

}

/**
 * Print Selected Thumbnail Images
 * @return string|bool thumbnail html or false
 */
function npgw_thumbnail( $size = 'post-thumbnail', $attr = array() ) {

	// Fallback to featured image
	if ( ! $thumbnail_id = get_post_meta( get_the_ID(), get_post_type() . "_{$size}_thumbnail_id", true ) ) {

		if ( ! $thumbnail_id = get_post_thumbnail_id() )
			return false;

	}

	// Get the thumbnail
	$thumbnail = apply_filters( 'post_thumbnail_html', wp_get_attachment_image( $thumbnail_id, $size, false, $attr ), get_the_ID(), $thumbnail_id, $size, $attr );

	// Return with thumbnail filter for lazy loading
	return "<div class=\"thumbnail\">\n{$thumbnail}\n</div>";

}