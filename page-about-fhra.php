<?php
/**
 * Page Template - About FHR
 * @package NPGW
 * @since 0.0.1
 */

get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post();

$npgw_about_fhr_production = get_post_meta( $post->ID, 'npgw_about_fhr_production', true );
$npgw_about_fhr_community = get_post_meta( $post->ID, 'npgw_about_fhr_community', true );
$npgw_about_fhr_safety = get_post_meta( $post->ID, 'npgw_about_fhr_safety', true );
$npgw_about_fhr_agencies = get_post_meta( $post->ID, 'npgw_about_fhr_agencies', true );
$npgw_about_fhr_approach = get_post_meta( $post->ID, 'npgw_about_fhr_approach', true );
$npgw_about_fhr_response = get_post_meta( $post->ID, 'npgw_about_fhr_response', true );


?>

<div class='container'>

	<div class='block about'>

		<ul class='info'>
			<li class='oil'>
				<strong>Production</strong>
				<?php echo wpautop( wp_kses_post( $npgw_about_fhr_production ) ); ?>
			</li>
			<li class='group'>
				<strong>Community</strong>
				<?php echo wpautop( wp_kses_post( $npgw_about_fhr_community ) ); ?>
			</li>
			<li class='safety'>
				<strong>Safety</strong>
				<?php echo wpautop( wp_kses_post( $npgw_about_fhr_safety ) ); ?>
			</li>
		</ul>

		<h3>Local Agencies Supported by FHRA</h3>

		<div class='agencies'>
			<?php echo wp_kses_post( $npgw_about_fhr_agencies ); ?>
		</div>

	</div>

	<div class='block history'>

		<h2 class='section-title'><span>History</span></h2>

		<ul>

		<?php $history_query = new WP_Query( array(
			'post_type' => 'npgw-history',
			'posts_per_page' => '50',
			'order' => 'ASC'
		) );

		if ( $history_query->have_posts() ) : while ( $history_query->have_posts() ) : $history_query->the_post(); ?>

			<li <?php post_class(); ?>>
				<?php echo the_post_thumbnail(); ?>
				<h4><?php the_title(); ?></h4>
				<?php the_content(); ?>
			</li>

		<?php endwhile; endif; wp_reset_query(); ?>

		</ul>
	</div>


	<div class='block approach-response'>

		<div class='approach'>

			<h2>FHRA Approach</h2>

			<?php echo wpautop( wp_kses_post( $npgw_about_fhr_approach ) ); ?>

		</div>

		<div class='response'>

			<h2>Response</h2>

			<?php echo wpautop( wp_kses_post( $npgw_about_fhr_response ) ); ?>

		</div>

	</div>

	<div class='content article-about-fhr'>
		<div class='inner'>

			<?php the_content(); ?>

		</div>
	</div>
</div>

<?php endwhile; endif; wp_reset_query();

get_footer();