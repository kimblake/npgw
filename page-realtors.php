<?php
/**
 * Index Template
 * @package NPGW
 * @since 0.0.1
 */

get_header(); ?>

<div class='outer'>
<div class='container'>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div <?php post_class(); ?>>

		<?php the_title( '<h1 class=\'page-title\'>', '</h1>' ); ?>

		<div class='content'>

			<?php the_content(); ?>

		</div>

	</div>

<?php endwhile; endif; wp_reset_query(); ?>

</div>
</div>
<div class='container'>

<h3 class='center-title'>Private Wells Map</h3>
<img src='<?php echo npgw_private_wells_map_url(); ?>' alt='Private Wells Map' />

<?php
// Check for ContactForm 7 Realtor Signup Form
if ( get_post_meta( $post->ID, 'realtor-signup-form', true ) ) {
	echo do_shortcode( get_post_meta( $post->ID, 'realtor-signup-form', true ) );
};

?>
</div>

<?php get_footer();