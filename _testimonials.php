<?php
/**
 * Testimonials Template Partial
 * @package NPGW
 * @since 0.0.1
 */ ?>

<div class='testimonials'>

	<h2 class='section-title'>Testimonials</h2>

	<div class='large-testimonial'>

	<?php 

	$testimonial_query = new WP_Query( array(
		'post_type' => 'npgw-testimonial',
		'posts_per_page' => 6
	) );

	if ( $testimonial_query->have_posts() ) : while ( $testimonial_query->have_posts() ) : $testimonial_query->the_post();

		$testimonial_content = get_the_content();
		$testimonial_attr = array( 'alt' => $testimonial_content );
		$testimonial_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
		$testimonial_image = apply_filters( 'post_thumbnail_html', wp_get_attachment_image( $testimonial_thumbnail_id, 'testimonial-image', false, $testimonial_attr ), get_the_ID(), $testimonial_thumbnail_id, 'testimonial-image', $testimonial_attr );

		echo $testimonial_image;

		if ( ! isset( $npgw_testimonial_first_post_switch ) ) :

			$npgw_testimonial_first_post_switch = true;
			$large_testimonial_content = $testimonial_content; ?>

			</div>

			<div class='small-testimonials'>

			<?php echo $testimonial_image;

		endif; ?>

	<?php endwhile; endif; wp_reset_query(); ?>

	</div>

	<blockquote class='testimonial-quote'>
		&quot;<?php echo $large_testimonial_content; ?>.&quot;
	</blockquote>

</div>