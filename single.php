<?php
/**
 * Single Post Template
 * @package NPGW
 * @since 0.0.1
 */

get_header(); ?>

<div class='outer'>
<div class='container'>
<div class='loop'>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div <?php post_class(); ?>>

		<?php the_title( '<h1 class=\'page-title\'>', '</h1>' ); ?>

		<div class='content'>

			<?php the_content(); ?>

		</div>

		<?php comments_template(); ?>

	</div>

<?php endwhile; endif; wp_reset_query(); ?>

</div>

<?php get_sidebar(); ?>

</div>
</div>

<?php get_footer();