<?php
/**
 * Header Template
 * @package NPGW
 * @since 0.0.1
 */ ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html <?php language_attributes(); ?> class='no-js ie6 lt-ie9 lt-ie8 lt-ie7'><![endif]-->
<!--[if IE 7 ]><html <?php language_attributes(); ?> class='no-js ie7 lt-ie9 lt-ie8'><![endif]-->
<!--[if IE 8 ]><html <?php language_attributes(); ?> class='no-js ie8 lt-ie9'><![endif]-->
<!--[if IE 9 ]><html <?php language_attributes(); ?> class='no-js ie9 lte-ie9'><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class='no-js'><!--<![endif]-->
<head>
<meta charset='utf-8'>
<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<?php wp_head(); ?>
</head>
<body id='npgw' <?php body_class(); ?>>
<div id='header' role='banner'>
	<div class='container'>
		<a class='header-link' href='/' rel='home'>
			<div id='logo'></div>
			<h1>Sulfolane Awareness</h1>
			<img class="home" src='<?php echo npgw_image( 'home-circle.png' ); ?>' alt='Home' />
		</a>

		<span class='menu-button button'>Menu</span>

		<?php wp_nav_menu( 'primary' ); ?>
	</div><!-- .container -->

	<?php if ( npgw_is_feature_page() ) : ?>

	<div id='header-image'>

		<?php if ( is_front_page() ) : ?>
		
		<h2><span><?php bloginfo( 'description' ); ?></span></h2>
		
		<?php endif; ?>

	</div><!-- #header-image -->

		<?php if ( is_front_page() ) :

			$npgw_home_welcome = get_post_meta( get_the_ID(), 'npgw_home_welcome', true ); ?>

			<h3><?php echo wp_kses_post( $npgw_home_welcome ); ?></h3>

		<?php endif;

endif; ?>

</div><!-- #header -->
<div class='main' role='main'>
