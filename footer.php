<?php
/**
 * Footer Template
 * @package NPGW
 * @since 0.0.1
 */ ?>

</div>
<div id='footer' role='contentinfo'>
	
	<p>&copy; <?php echo date("Y") ?> Flint Hills Resources Alaska</p>

</div>

<?php wp_footer(); ?>

</body>
</html>