<?php
/**
 * Theme functions
 *
 * @package NPGW
 * @author 10up / Jeff Sebring <jeff@10up.com>
 * @version Release: 0.0.1
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPLv3
 * @link http://10up.com
 */

// Instantiate!
NPGW::instance();

class NPGW {

	/**
	 * Instance
	 * @var NPGW
	 * @access protected
	 */
	protected static $_instance;

	/**
	 * Singleton instance
	 * @return NPGW
	 */
	public static function instance() {

		if ( ! isset( self::$_instance ) )
			self::$_instance = new self;

		return self::$_instance;

	}

	/**
	 * Theme constructor
	 * @access protected
	 */
	protected function __construct() {

		// Admin functions
		if ( is_admin() )
			require_once __DIR__ . '/includes/class-admin.php';

		// Template tags
		require_once __DIR__ . '/includes/template-tags.php';

		// Content width
		global $content_width;
		isset( $content_width ) || $content_width = 960;

		// Filters
		add_filter( 'body_class', array( $this, 'body_class' ) );
		add_filter( 'post_class', array( $this, 'post_class' ) );
		add_filter( 'excerpt_length', array( $this, 'excerpt_length' ), 999 );
		add_filter( 'use_default_gallery_style', '__return_false' );

		// Actions
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'after_setup_theme', array( $this, 'setup' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'assets' ) );
		add_action( 'wp_print_styles', array( $this, 'styles_version' ), 100 );
		add_action( 'wp_print_scripts', array( $this, 'scripts_version' ), 100 );
		add_action( 'wp_print_footer_scripts', array( $this, 'scripts_version' ), 100 );
		add_action( 'widgets_init', array( $this, 'remove_recent_comments_style' ) );

	}

	/**
	 * Post class filter
	 * @return array $classes
	 * @access public
	 */
	public function body_class( $classes ) {

		global $post;

		if ( is_singular() )
			$classes[] = 'page-' . $post->post_name;

		return $classes;

	}

	/**
	 * Post class filter
	 * @return array $classes
	 * @access public
	 */
	public function post_class( $classes ) {

		global $post;

		$classes[] = 'article';
		$classes[] = 'article-' . $post->post_name;

		return $classes;

	}

	/**
	 * Filter excerpt length for pages
	 * @return array $classes
	 * @access public
	 */
	public function excerpt_length( $length ) {

		if ( is_page() )
			return 9999; 

		return $length;

	}

	/**
	 * Scripts & styles
	 * @return void
	 * @access public
	 */
	public function assets() {

		$template_directory_uri = get_template_directory_uri();

		wp_enqueue_style(
			'npgw-fonts',
			'//fonts.googleapis.com/css?family=Raleway:700,300'
		);

		wp_enqueue_style(
			'npgw-style',
			$template_directory_uri . '/assets/css/min/style.css'
		);

		wp_enqueue_script(
			'npgw-header',
			$template_directory_uri . '/assets/js/min/header.js'
		);

		wp_enqueue_script(
			'npgw-contact',
			$template_directory_uri . '/assets/js/min/contact.js',
			'',
			'',
			true
		);

		wp_enqueue_script(
			'npgw-footer',
			$template_directory_uri . '/assets/js/min/footer.js',
			array( 'jquery' ),
			'',
			true
		);

	}

	/**
	 * Theme init
	 * @return void
	 * @access public
	 */
	public function init() {

		// Testimonial post type
		register_post_type( 'npgw-testimonial', array(
			'labels' => array(
				'name' => 'Testimonials',
				'singular_name' => 'Testimonial',
				'add_new' => 'Add New',
				'add_new_item' => 'Add New Testimonial',
				'edit_item' => 'Edit Testimonial',
				'new_item' => 'New Testimonial',
				'all_items' => 'All Testimonials',
				'view_item' => 'View Testimonial',
				'search_items' => 'Search Testimonials',
				'not_found' => 'No Testimonials found',
				'not_found_in_trash' => 'No Testimonials found in Trash',
				'parent_item_colon' => '',
				'menu_name' => 'Testimonials'
			),
			'public' => true,
			'exclude_from_search' => true,
			'publicly_queryable' => false,
			'menu_position' => 20,
			'rewrite' => array( 'slug' => 'testimonial' ),
			'label' => 'Testimonials',
			'hierarchical' => true,
			'supports' => array(
				'thumbnail',
				'editor',
				'title',
				'page-attributes'
			)
		) );

		// History post type
		register_post_type( 'npgw-history', array(
			'labels' => array(
				'name' => 'History',
				'singular_name' => 'History',
				'add_new' => 'Add New',
				'add_new_item' => 'Add New History',
				'edit_item' => 'Edit History',
				'new_item' => 'New History',
				'all_items' => 'All History',
				'view_item' => 'View History',
				'search_items' => 'Search History',
				'not_found' => 'No History found',
				'not_found_in_trash' => 'No History found in Trash',
				'parent_item_colon' => '',
				'menu_name' => 'History'
			),
			'public' => true,
			'exclude_from_search' => true,
			'publicly_queryable' => false,
			'menu_position' => 20,
			'rewrite' => array( 'slug' => 'history' ),
			'label' => 'History',
			'hierarchical' => true,
			'supports' => array(
				'thumbnail',
				'editor',
				'title',
				'page-attributes'
			)
		) );

		// Testimonial image size
		add_image_size( 'testimonial-image', 350, 350, true );
		
		

		// Featured images
		add_theme_support( 'post-thumbnails', array( 'post', 'npgw-testimonial', 'npgw-history' ) );

		// Page Excerpts
		add_post_type_support( 'page', 'excerpt' );

	}

	/**
	 * Theme setup
	 * @return void
	 * @access public
	 */
	public function setup() {

		// Menu
		register_nav_menu( 'primary', 'Primary' );

		register_sidebar( array(
			'id' => 'sidebar',
			'name' => 'Sidebar',
			'description' => 'Blog sidebar',
			'before_widget' => "\n\t\t<section class=\"widget %2\$s\">\n",
			'after_widget' => "\n\t\t</section>\n",
			'before_title' => "\n<h4>",
			'after_title' => "</h4>\n"
		) );
		
		
		register_sidebar( array(
			'id' => 'resources',
			'name' => 'Resources',
			'description' => 'Resources sidebar',
			'before_widget' => "\n\t\t<section class=\"widget %2\$s\">\n",
			'after_widget' => "\n\t\t</section>\n",
			'before_title' => "\n<h4>",
			'after_title' => "</h4>\n"
		) );


		// Clean document head
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'wlwmanifest_link' );
		remove_action( 'wp_head', 'index_rel_link' );
		remove_action( 'wp_head', 'wp_generator' );
		remove_action( 'wp_head', 'feed_links_extra', 3 );
		remove_action( 'wp_head', 'feed_links', 2 );
		remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
		remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
		remove_action( 'wp_head', 'start_post_rel_link_wp_head', 10, 0 );
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

	}

	/**
	 * Remove recent comment style
	 * @return void
	 * @access public
	 */
	public function remove_recent_comments_style() {

		global $wp_widget_factory;

		remove_action(
			'wp_head',
			array(
				$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
				'recent_comments_style'
			)
		);  

	}

	/**
	 * Change script version parameter from WP version to theme version
	 * @access public
	 */
	public function scripts_version() {

		global $wp_scripts;

		if ( ! is_a( $wp_scripts, 'WP_Scripts' ) ) return;

		foreach ( $wp_scripts->registered as $handle => $script )
			$wp_scripts->registered[$handle]->ver = 'npgw';

	}

	/**
	 * Change stylesheet version parameter from WP version to theme version
	 * @access public
	 */
	public function styles_version() {

		global $wp_styles;

		if ( ! is_a( $wp_styles, 'WP_Styles' ) ) return;

		foreach ( $wp_styles->registered as $handle => $style )
			$wp_styles->registered[$handle]->ver = 'npgw';

	}

}

/**
 * Manually place Yoast Analytics code with our own doubleclick.net version
 */
function npgw_manual_yoast_analytics() {
	$options = get_option( 'Yoast_Google_Analytics' );
	$uastring = $options['uastring'];

	// If Yoast position is set to manual
	if ( $options['position'] == 'manual' && !empty($uastring) ) { ?>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', '<?php echo esc_js( $uastring ); ?>', 'northpolegroundwater.com');
			ga('send', 'pageview');

		</script>
	<?php } //end check
}

add_action( 'wp_head', 'npgw_manual_yoast_analytics');

function npgw_mailchimp_shortcode_handler($atts) {

	ob_start();
	?>
	<!-- Begin MailChimp Signup Form -->
	<div id='mc_embed_signup'>
		<form action='http://fhr.us3.list-manage.com/subscribe/post?u=55e9e598f384e73bd184ab325&amp;id=87f51454c6' method='post' id='mc-embedded-subscribe-form' name='mc-embedded-subscribe-form' class='validate' target='_blank' novalidate>
			<h3>For email newsletters:</h3>
			<div class='form'>
				<div class='indicates-required'><span class='asterisk'>*</span> indicates required</div>
				<div class='mc-field-group'>
					<label for='mce-EMAIL'>Email Address  <span class='asterisk'>*</span></label>
					<input placeholder='Email Address *' type='email' value='' name='EMAIL' class='required email' id='mce-EMAIL'>
				</div>
				<div class='mc-field-group'>
					<label for='mce-FNAME'>First Name </label>
					<input placeholder='First Name' type='text' value='' name='FNAME' class='' id='mce-FNAME'>
				</div>
				<div class='mc-field-group'>
					<label for='mce-LNAME'>Last Name </label>
					<input placeholder='Last Name' type='text' value='' name='LNAME' class='' id='mce-LNAME'>
				</div>
				<div id='mce-responses' class='clear'>
					<div class='response' id='mce-error-response' style='display:none'></div>
					<div class='response' id='mce-success-response' style='display:none'></div>
				</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
				<div style='position: absolute; left: -5000px;'><input type='text' name='b_55e9e598f384e73bd184ab325_87f51454c6' value=''></div>
				<div class='clear'>
					<input type='submit' value='Sign Up' name='subscribe' id='mc-embedded-subscribe' class='button'>
				</div>
			</div>
		</form>
	</div>
	<?php

	return ob_get_clean();
}

add_shortcode( 'npgw-mailchimp-form', 'npgw_mailchimp_shortcode_handler' );
