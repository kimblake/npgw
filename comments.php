<?php
/**
 * Comments Template
 * @package NPGW
 * @since 0.0.1
 */

// Get number of comment pages
$comment_pages_count = get_comment_pages_count();
$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );

// Only display form if comments are open
if ( comments_open() ) :

	comment_form( array(
		'format' => 'html5',
		'comment_notes_after' => '',
		'title_reply' => 'Leave a comment.',
		'comment_notes_after' => '',
		'label_submit' => 'Comment',
		'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) .
		'</label><textarea id="comment" name="comment" placeholder="Comment" cols="45" rows="8" aria-required="true">' .
		'</textarea></p>',
		'fields' => array(
			'author' => '<p class="comment-form-author"><label for="author">Name</label> ' .
			'<input id="author" name="author" type="text" placeholder="Name" value="' . esc_attr( $commenter['comment_author'] ) .
			'" size="30"' . $aria_req . ' /></p>',
			'email' => '<p class="comment-form-email"><label for="email">Email</label> ' .
			'<input id="email" name="email" type="text" placeholder="Email" value="' . esc_attr(  $commenter['comment_author_email'] ) .
			'" size="30"' . $aria_req . ' /></p>',
			'url' => '<p class="comment-form-url"><label for="url">Website</label>' .
			'<input id="url" name="url" type="text" placeholder="Website" value="' . esc_attr( $commenter['comment_author_url'] ) .
			'" size="30" /></p>',
		)
	) );

elseif ( isset( $comments ) ) : ?>

	<em class="closed_comments">Comments are closed.</em>

<?php endif;

if ( have_comments() ) : ?>

<section class="comments">

	<div class="comment_list">
		<?php wp_list_comments( array(
			'style' => 'div',
			'format' => 'html5',
			'type' => 'comment',
			'callback' => 'npgw_comments'
		) ); ?>
	</div>

</section>

<?php endif;