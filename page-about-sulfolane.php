<?php
/**
 * Page Template - Contact
 * @package NPGW
 * @since 0.0.1
 */

get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class='outer'>
<div class='container'>
<div class='content excerpt clearfix'>

	<?php the_excerpt(); ?>

</div>
</div>
</div>

<div class='content'>

	<?php the_content(); ?>

</div>

<?php endwhile; endif; wp_reset_query();

get_footer();