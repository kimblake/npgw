# Compass Configuration

http_path = "/"
css_dir = "/assets/css/min"
fonts_dir = "/assets/fonts"
sass_dir = "/assets/css/src"
images_dir = "/assets/images"
javascripts_dir = "/assets/js"
output_style = :compressed