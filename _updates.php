<?php
/**
 * Updates Template Partial
 * @package NPGW
 * @since 0.0.1
 */ ?>

<div class='updates'>

	<h2 class='section-title'>Updates</h2>

	<?php $updates_query = new WP_Query( array(
		'posts_per_page' => 5,
		'post_type' => 'post',
		'ignore_sticky_posts' => 1
	) );

	if ( $updates_query->have_posts() ) :
		
		while ( $updates_query->have_posts() ) :

			$updates_query->the_post(); ?>

	<a href='<?php the_permalink(); ?>'>
		<h1><?php the_title(); ?></h1>
		<time><?php the_date(); ?></time>
	</a>

	<?php 	endwhile;

	endif;

	wp_reset_query(); ?>

</div>
