<?php
/**
 * Sidebar Template
 * @package NPGW
 * @since 0.0.1
 */ ?>

 <aside class='sidebar' role='complementary'>

 	<?php dynamic_sidebar( 'sidebar' ); ?>

 </aside>