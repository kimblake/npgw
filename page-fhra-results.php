<?php
/**
 * Page Template - FHRA Results
 * @package NPGW
 * @since 0.0.1
 */

get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div <?php post_class( 'container' ); ?>>


	<div class='content'>

		<?php the_content(); ?>
	
	</div>

</div>

<?php endwhile; endif; wp_reset_query();

get_footer();