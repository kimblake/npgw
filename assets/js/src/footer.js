jQuery( document ).ready( function() {

	jQuery( '.menu-button' ).on( 'click', function( event ) {

		jQuery( '#header .menu-primary-container' ).toggle();

	} );

	jQuery( '#header #menu-primary' ).prepend( '<li class="home"><a href="/">Home</a></li>' );

	jQuery( '.content' ).fitVids();

	jQuery( '.small-testimonials img' ).on( 'click', function( event ) {

		jQuery( '.large-testimonial img' ).attr( 'src', jQuery( this ).attr( 'src' ) );
		jQuery( '.testimonial-quote' ).html( jQuery( this ).attr( 'alt' ) );

	} );

});