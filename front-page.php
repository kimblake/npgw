<?php
/**
 * Front Page Template
 * @package NPGW
 * @since 0.0.1
 */

get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div class='container'>

		<div <?php post_class( 'block' ); ?>>

			<div class='illustration'>
				<h2>What is Sulfolane?</h2>
				<h3>Chemical compound</h3>
				<img src='<?php echo npgw_image( 'illus_sulfolane.jpg' ); ?>' alt='Image of Sulfolane chemical compound and uses.' />
			</div>

			<div class='content'>
				<?php the_content(); ?>
			</div>

		</div>

		<div class='mid-wrap'>

			<?php // get_template_part( '_testimonials' ); ?>

			<?php get_template_part( '_updates' ); ?>
		
		</div>

		<div class='water-trends'>

			<h2>2013 Water Trends</h2>

			<img src='<?php echo npgw_image( 'water-trends-map.png' ); ?>' />

		</div>

	</div>

<?php endwhile; endif; wp_reset_query();

get_footer();